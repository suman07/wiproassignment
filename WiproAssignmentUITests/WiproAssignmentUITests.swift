//
//  WiproAssignmentUITests.swift
//  WiproAssignmentUITests
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import XCTest

class WiproAssignmentUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
        
    func testViewController_PulltoRefreshTest() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
       XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .navigationBar).matching(identifier: "About Canada").element(boundBy: 1).buttons["Refresh"].tap()
        // For Example purpose
          let refresh = app.buttons["Refresh"]
        XCTAssertTrue(refresh.isEnabled, "The Rfresh button is enabled for user interaction")
        //act
        refresh.tap()
        // assert
        XCTAssertTrue(app.alerts["errorAlert"].waitForExistence(timeout: 1.0), "An error alert dialog was not presented when api call was failed")
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
