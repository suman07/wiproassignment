//
//  ViewController.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import UIKit
import SnapKit
class ViewController: UIViewController {
    
    
    var arrRowdata = [RowViewModel]()
    lazy var containerView = UIView()
    lazy var tableView = UITableView()
    var navBarHeight = 44
    var gap = CGFloat ()
    var navTitle : String? = nil
    var navigationBar = UINavigationBar()
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// Using snapkit framework programetically creating a view and adding tableview,  Here i adding leading, trailing,top, bottom,height and width.
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.gap = 10.0
        self.view.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.left.right.equalTo(self.view)
            make.top.equalTo(navBarHeight)
            make.top.equalTo(self.view.snp.top).offset(navBarHeight)
            make.bottom.equalTo(self.view.snp.bottom).offset(0)
        }
        containerView.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            
            make.top.equalTo(gap)
            make.height.equalTo(self.view.snp.height).multipliedBy(0.89)
            make.left.equalTo(self.view.snp.left).offset(gap)
            make.right.equalTo(self.view.snp.right).offset(-gap)
            tableView.backgroundColor = UIColor.clear
            self.tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.estimatedRowHeight = 50
            tableView.allowsSelection = true
            tableView.tableFooterView = UIView()
            self.tableView.isUserInteractionEnabled = true
        }
        
      
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
          self.networkCall()
    }
    // MARK: NetworkCall
    /// returns: string , array of dictionary, and Error.
    func networkCall() {
        let status = Reach().connectionStatus()
            switch status {
        
            case .unknown, .offline:
                self.createAlert(strMessage: ALERT_ZEROCONNECTIONERROR)
           
            case .online(.wwan):
                fallthrough
            case .online(.wiFi):
                let activityView = UIActivityIndicatorView(style: .medium)
                            activityView.color = UIColor.black
                               activityView.center = self.view.center
                               self.view.addSubview(activityView)
                               activityView.startAnimating()
                            self.view.isUserInteractionEnabled = false
                            Service.shred.getdatafromServer {
                                (data, Title, error) in
                                if let err = error {
                                      DispatchQueue.main.async {
                                   self.view.isUserInteractionEnabled = true
                                   activityView.removeFromSuperview()
                                        self.createAlert(strMessage: err.localizedDescription)
                                    }
                                               return
                                           }

                                if data != nil
                                {
                                   
                                    self.arrRowdata =  data?.map({return RowViewModel(rowModeldata: $0)}) ?? []
                                    print(self.arrRowdata.count)
                                    DispatchQueue.main.async {
                                        self.navTitle = Title
                                        self.navigationBar.topItem?.title = self.navTitle
                                        self.tableView.reloadData()
                                        self.view.isUserInteractionEnabled = true
                                        activityView.removeFromSuperview()
                                    }
                                }
                                else
                                {
                                    self.createAlert(strMessage: ALERT_NODATA)
                                }
                                
                            }
                                            }
        
        
 
    }
    /// Create a alert controller
    func createAlert(strMessage : String)
    {
       // create the alert
        let alert = UIAlertController(title: "Error", message: strMessage, preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        DispatchQueue.main.async {
            alert.view.accessibilityIdentifier = "errorAlert"
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    ///
    /// Create NavigationBar and Navigation item.
    override func viewWillLayoutSubviews() {
        let width = self.view.frame.width
        navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 20, width: width, height: 44))
        self.view.addSubview(navigationBar);
        let navigationItem = UINavigationItem(title: self.navTitle ?? "")
        let doneBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: nil, action: #selector(buttonRefresh))
        navigationItem.rightBarButtonItem = doneBtn
        navigationBar.setItems([navigationItem], animated: false)
        
    }
    
    /// Call webservicce
    @objc func buttonRefresh() {
        self.networkCall()
    }
}
 // MARK: TableViewDelegate,TableViewDataSource Method
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRowdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        let data = self.arrRowdata[indexPath.row]
       // print(data.imageHref)
        cell.iconImageView.loadImageUsingCache(withUrl: data.imageHref ?? "")
        cell.labelTitle.text = data.title
        // upate labelTitle height according to the text
        cell.labelTitle.snp.updateConstraints{ (make) in
            
            make.height.equalTo(cell.labelTitle.heightForLabel(text:  data.title ?? "", font: BoldFont17, width: cell.contentView.frame.width))
            
        }
        cell.labelTitle.font = BoldFont17
        cell.labelDbescription.text = data.description
        cell.labelDbescription.font = BoldFont17
        cell.labelDbescription.snp.updateConstraints{ (make) in
            
            make.top.equalTo(cell.labelTitle.snp.bottom).offset(1)
            make.bottom.equalTo(cell.contentView.snp.bottom)
        }
        return cell
    }
}


