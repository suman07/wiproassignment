//
//  TableViewCell.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
class TableViewCell: UITableViewCell {
    static let identifier: String = "Cell"

    var labelTitle: UILabel!
     var labelDbescription: UILabel!
    var iconImageView : UIImageView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.configure()
    }

    
    
    func configure() {
        iconImageView = UIImageView(frame: .zero)
        self.contentView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(10).priorityRequired()
            make.right.equalTo(self.contentView).offset(-10).priorityRequired()
            make.top.equalTo(self.contentView.snp.top).offset(10).priorityLow()
            make.height.equalTo(self.contentView.snp.height).multipliedBy(0.5).priorityRequired()
        }
        labelTitle = UILabel(frame: .zero)
        self.contentView.addSubview(labelTitle)
        labelTitle.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentView).offset(-10).priorityRequired()
            make.left.equalTo(self.contentView).offset(10).priorityRequired()
            make.top.equalTo(self.iconImageView.snp.bottom).offset(5).priorityRequired()
            make.height.equalTo(20)
        }
        labelTitle.font = BoldFont17
        labelTitle.numberOfLines = 0
        labelDbescription = UILabel(frame: .zero)
        self.contentView.addSubview(labelDbescription)
        labelDbescription.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentView).offset(-10).priorityRequired()
            make.left.equalTo(self.contentView).offset(10).priorityRequired()
            make.top.equalTo(self.labelTitle.snp.bottom).offset(5).priorityRequired()
            make.bottom.equalTo(self.contentView.snp.bottom).offset(5).priorityRequired()
        }
        labelDbescription.font = BoldFont17
        labelDbescription.numberOfLines = 0
    }
       
        

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
