//
//  RowViewMode.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
/// Create a dependency injection.
struct RowViewModel {
    var title : String? = nil
    var imageHref : String? = nil
    var description : String? = nil
    init(rowModeldata : RowsModel) {
        self.title = rowModeldata.title
        self.description = rowModeldata.description
        self.imageHref = rowModeldata.imageHref
    }
}
