//
//  Mode.swift
//  WiproAssignment
//
//  Created by Sudipta on 25/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
///Using for  fetching arrayofdictionary from json.
struct RowsModel : Decodable
{
    var title : String? = nil
    var imageHref : String? = nil
    var description : String? = nil
    init(title : String,imageHref : String,description : String) {
        self.imageHref = imageHref
        self.title = title
        self.description = description
    }
}

/// Extract json
struct DataModel : Decodable {
    var rows = [RowsModel]()
    var title : String? = nil
    init(rows : [RowsModel],title : String) {
        self.rows = rows
        self.title = title
    }
}

