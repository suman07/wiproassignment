//
//  Service.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
/// class use for network calling, it's  a shared class.
class Service : WebServiceProtocol {
    static let shred = Service()
    private init()
    {
        
    }
    fileprivate static let syncQueue = DispatchQueue(label: "Service.syncQueue")
    /**
     Summay text for { () -> rowdata, Title,Error
     
     }
     
     - parameter :  get method.
     - returns: string and array of dictionary
     - warning: Instead of json format, it's return as a text format.
     # Notes: #
     1. Need to convert text format to json format.
     2. For thread safety  using serial que.
     
     */
    
    func getdatafromServer(completion:@escaping([RowsModel]?,String?, Error?)->()){
        Service.syncQueue.sync {
            let url = kBaseURL
            let apiurl = URL(string: url)!
            URLSession.shared.dataTask(with: apiurl) { (data, respone, error) in
                if let err = error
                {
                    print("error\(error?.localizedDescription ?? "")")
                    completion(nil,nil,(err.localizedDescription as? Error))
                }
                else
                {
                    guard let data = data else{return}
                    guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                    guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                    do {
                        let jsonresult = try JSONDecoder().decode(DataModel.self, from: properData)
                        var arrRows = [RowsModel]()
                        print(jsonresult.rows.count)
                        for data in jsonresult.rows
                        {
                            
                                
                                arrRows.append(RowsModel.init(title: data.title ?? "", imageHref: data.imageHref ?? "", description:data.description ?? ""))
                                
                        }
                        completion(arrRows,jsonresult.title ?? "", nil)
                        
                        
                    } catch let jsonerror {
                        print(jsonerror)
                        completion(nil,nil,jsonerror)
                    }
                }
            }.resume()
        }
    }
    
    
}
