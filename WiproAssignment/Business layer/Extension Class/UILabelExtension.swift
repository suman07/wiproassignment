//
//  UILabelExtension.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import Foundation
import UIKit
/// According to  the text size, will calculate the label height.
extension UILabel
{
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = font
        label.text = text
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        return label.frame.height
    }
}
