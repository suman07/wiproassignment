//
//  Constant.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//
import Foundation
import UIKit

/// Base Url for network calling
var kBaseURL = String("https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")

/// Define font name and size
let BoldFont17 =  UIFont.boldSystemFont(ofSize: 17)
let ALERT_ZEROCONNECTIONERROR = "No Internet connection"
let ALERT_NODATA = "No data found"

