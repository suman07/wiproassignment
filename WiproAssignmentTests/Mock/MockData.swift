//
//  MockData.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import XCTest
@testable import WiproAssignment
class MockData: XCTestCase {
   
    func getData() -> Data {
        guard let data = self.readJson(forResource: "facts") else {
            XCTAssert(false, "Can't get data from  facts.json")
            return Data()
        }
        return data
    }

    

   

}
extension MockData {
    func readJson(forResource fileName: String ) -> Data? {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: fileName, withExtension: "json") else {
            XCTFail("Missing file: \(fileName).json")
            return nil
        }

        do {
            let data = try Data(contentsOf: url)
            return data
        } catch (_) {
            XCTFail("unable to read json")
            return nil
        }
    }
}
