//
//  ViewControllerTest.swift
//  WiproAssignment
//
//  Created by Sudipta on 24/09/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import XCTest
@testable import WiproAssignment
class ViewControllerTest: XCTestCase {
     var sut : ViewController!
    override func setUpWithError() throws {
        sut = ViewController()
        sut.loadViewIfNeeded()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    func testViewController_WhenCreated_HasButton() throws {
          // Arrange
          let _: UIBarButtonItem = try XCTUnwrap(sut.navigationItem.rightBarButtonItem, "Button does not have a referencing outlet")
          

      }

}
